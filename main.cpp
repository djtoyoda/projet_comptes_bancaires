#include <iostream>
#include <string>
#include <vector>
#include "Clients.h"
#include "Compte.h"
#include "Particuliers.h"
#include "Professionels.h"
#include "Exceptions.h"
#include "Tout.h"
using namespace std;

int main()
{
    try
    {
        Tout tout;
        Clients* part1 = new Particuliers("Jackson", "John", 'V', 8983188, "Blv", "Emile Zola", 31300, "Toulouse", Sit_fam::Autre, 2879219);
        Clients* pro1 = new Professionels("Renault", "Mike", 'X', 84984561, "Rue", "Flambere", 32700, "Bordeau", "15676596368956", "AKKA", 1980, "akka@akka.eu", "Blvd", "Thomas Ziegler", 31300, "Beauzelle");

        tout.ajoute_client(part1);
        tout.ajoute_client(pro1);

        Compte* c1 = new Compte(123, 7032020, 50.5, 0);
        Compte* c2 = new Compte(568, 8062019, 100.5, 100);
        Compte* c3 = new Compte(698, 9506807, 120, 50);

        part1->ajoute_compte(c1);
        pro1->ajoute_compte(c2);
        pro1->ajoute_compte(c3);

        c3->depot(150);
        c1->retrait(15);
        c2->paimentCB(10.5);

        cout << "--------------------------------------------------------------" << endl;
        cout << "%                                                            %" << endl;
        cout << "%                                                            %" << endl;
        cout << "%                       GESTION DE COMPTES                   %" << endl;
        cout << "%                                                            %" << endl;
        cout << "%                                                            %" << endl;
        cout << "--------------------------------------------------------------" << endl << endl;
        cout << "----------------------------- MENU ---------------------------" << endl;
        cout << "Choisissez l'option:" << endl;
        cout << "1- Lister l'ensemble des clients" << endl;
        cout << "2- Consulter les soldes des comptes pour un client donne" << endl;
        cout << "3- Ajouter/Supprimer/Modifier un client" << endl;
        cout << "4- Ajouter/Supprimer/Modifier une operation" << endl;
        cout << "5- Afficher l'ensemble des operations pour un compte donne" << endl;
        cout << "6- Importer le fichier des operations bancaires" << endl << endl;
        cout << "Option: ";
        int inputX;
        cin >> inputX;

        while(inputX < 1 || inputX > 6)
        {
            cout << "Option invalide; choisissez entre 1 e 6: ";
            cin >> inputX;
        }

        if(inputX==1)
        {
            tout.affiche_tout();
        }
        if(inputX==2)
        {
            cout << "Donnez les nom du client: ";
            string input2X;
            //cin >> input2X;
            input2X = "Renault";
            int cons = tout.consult_client(input2X);
            cout <<  "Cons " << cons;
            //int comp[] = tout.consult_compte(cons);
        }
        if(inputX==3)
        {
            cout << "Choisissez l'option:" << endl;
            cout << "1- Ajouter un client" << endl;
            cout << "2- Supprimer un client" << endl;
            cout << "3- Modifier un client" << endl;
            cout << "Option: ";
            int input3X;
            cin >> input3X;

            while(input3X < 1 || input3X > 3)
            {
                cout << "Option invalide; choisissez entre 1 e 3: ";
                cin >> input3X;
            }

            if(input3X==1)
            {
                int input31X;
                string n, p;
                cout << "Client particulier ou contat professionel?" << endl;
                cout << "1- Particulier" << endl;
                cout << "2- Professionel" << endl;
                cout << "Option: ";
                cin >> input31X;

                if(input31X == 1)
                {
                    string n; string p; char s; int t; string lib; string c; int cp; string v; string sf; int d; Sit_fam x;
                    cout << "Nom: ";
                    cin >> n;
                    cout << "Prenom: ";
                    cin >> p;
                    cout << "Sex: ";
                    cin >> s;
                    cout << "Telephone: ";
                    cin >> t;
                    cout << "Libelle: ";
                    cin >> lib;
                    cout << "Complement: ";
                    cin >> c;
                    cout << "Code Postal: ";
                    cin >> cp;
                    cout << "Ville: ";
                    cin >> v;
                    cout << "Situation familiale: ";
                    cin >> sf;

                    if(sf == "C") { x = Sit_fam::Celibataire; } else {
                    if(sf == "M") { x = Sit_fam::Marie; } else {
                    if(sf == "D") { x = Sit_fam::Divorce; } else {
                            { x = Sit_fam::Autre; }
                    }}}

                    cout << "Date naissance (ddmmjjjj)";
                    cin >> d;
                    Clients* part = new Particuliers(n, p, s, t, lib, c, cp, v, x, d);
                    tout.ajoute_client(part);
                }
                if(input31X == 2)
                {
                    string n; string p; char s; int t; string lib; string c; int cp; string v; string si; string rs; int an; string m; string lib1; string c1; int cp1;
                    cout << "Nom: ";
                    cin >> n;
                    cout << "Prenom: ";
                    cin >> p;
                    cout << "Sex: ";
                    cin >> s;
                    cout << "Telephone: ";
                    cin >> t;
                    cout << "Libelle: ";
                    cin >> lib;
                    cout << "Complement: ";
                    cin >> c;
                    cout << "Code Postal: ";
                    cin >> cp;
                    cout << "Ville: ";
                    cin >> v;
                    cout << "Siret: ";
                    cin >> si;
                    cout << "Raison sociale: ";
                    cin >> rs;
                    cout << "Annee creation: ";
                    cin >> an;
                    cout << "Mail: ";
                    cin >> m;
                    cout << "Libelle: ";
                    cin >> lib1;
                    cout << "Complement: ";
                    cin >> c1;
                    cout << "Code Postal: ";
                    cin >> cp1;
                    Clients* pro = new Professionels(n, p, s, t, lib, c, cp, v, si, rs, an, m, lib1, c1, cp1);
                    tout.ajoute_client(pro);
                }

                if(input3X==2)
                {
                    string n;
                    cout << "Indiquer le client a supprimer" << endl;
                    cout << "Nom: ";
                    cin >> n;
                    tout.supp_client(n);
                }
                if(input3X==3)
                {
                }
            }
        }
        if(inputX==4)
        {
            cout << "Choisissez l'option:" << endl;
            cout << "1- Ajouter une operation" << endl;
            cout << "2- Supprimer une operation" << endl;
            cout << "3- Modifier une operation" << endl;
            int input4X;
            cin >> input4X;

                while(input4X < 1 || input4X > 3)
                {
                    cout << "Option invalide; choisissez entre 1 e 3: ";
                    cin >> input4X;
                }

                if(input4X == 1)
                {
                    float input41X;
                    int input41Y, num_c;
                    cout << "Indiquer le numero de compte" << endl;
                    cout << "Numero: ";
                    cin >>  num_c;
                    cout << "Indiquer le montant et le type d'operation" << endl;
                    cout << "Montant: ";
                    cin >> input41X;
                    cout << "1- Retrait" << endl;
                    cout << "2- Paiment CB" << endl;
                    cout << "3- Depot" << endl;
                    cin >> input41Y;
/*
                    if(input41Y == 1)
                    {
                        for(auto elm : tout)
                        {
                            if(elm->get_num_compte() == num_c)
                            {
                                elm->Comptes::retrait(input41X);
                            }
                        }
                    }
                    if(input41Y == 2)
                    {
                        for(auto elm : tout)
                        {
                            if(elm->get_num_compte() == num_c)
                            {
                                elm->Comptes::depot(input41X);
                            }
                        }
                    }
                    if(input41Y == 3)
                    {
                        for(auto elm : tout)
                        {
                            if(elm->get_num_compte() == num_c)
                            {
                                elm->Comptes::paimentCB(input31X);
                            }
                        }
                    }
                    */
                }

                if(input4X==2)
                {
                    float input42X;
                    cout << "Indiquer le numero de compte a supprimer" << endl;
                    cout << "Compte: ";
                    cin >> input42X;
/*                  for(auto elm : tout)
                    {
                        if(elm->get_num_compte() == input42X)
                        {
                            elm->Comptes::suppr_compte(input42X);
                        }
                    }
                    */
                }

                if(input4X==3)
                {
                }
        }

        if(inputX==5)
        {
            int input5X;
            cout << "Indiquer le numero de compte a afficher" << endl;
            cout << "Compte: ";
            cin >> input5X;
        }
    }
    catch(Exceptions& ex)
    {
        cout << ex.what() << endl;
    }
    return 0;
}
