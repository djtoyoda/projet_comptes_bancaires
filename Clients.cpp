#include <iostream>
#include <vector>
#include "Clients.h"
#include "Adresse.h"
#include "Compte.h"
#include "Particuliers.h"
#include "Professionels.h"
using namespace std;

Clients::Clients(string n, string p, char s, int t, string lib, string c, int cp, string v)
{
    this->set_nom(n);
    this->set_prenom(p);
    this->set_sex(s);
    this->set_telephone(t);
    adresse_client = new Adresse(lib, c, cp, v);
}

void Clients::affiche()
{
    cout << "Nom, prenom: " << this->get_nom() << ", " << this->get_prenom() << "; Sex: " << this-> get_sex() << ", Telephone: " << this->get_telephone() << endl;
    adresse_client->Adresse::affiche();

    cout << endl << "----------------- Nb Comptes : " << this->get_nb_comptes() << " comptes --------------" << endl << endl;
    for(auto elm : vecComptes)
    {
        elm->affiche();
        //elm->affiche_detail();
    }
    cout << endl << "--------------------xxx----------------------" << endl << endl;
}

int Clients::get_nb_comptes()
{
    return vecComptes.size();
}

void Clients::ajoute_compte(Compte* c)
{
    vecComptes.push_back(c);
}

void Clients::suppr_compte(Compte* c)
{
    //vecComptes.erase(0);
}

void Clients::affiche_detail()
{
//    this->Professionels::affiche_detail();
}

