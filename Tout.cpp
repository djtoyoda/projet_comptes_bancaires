#include "Tout.h"
#include <iostream>
#include "Professionels.h"

using namespace std;

void Tout::affiche_tout()
{
    cout << endl << "-------------------- Nb Clients : " << this->get_nb_client() << " clients -------------" << endl << endl;
    for(auto elm : vecClients)
    {
        elm->Clients::affiche();
        elm->Clients::affiche_detail();
        cout << endl << "---------------------------------------" << endl;
    }
}

void Tout::ajoute_client(Clients* c)
{
    vecClients.push_back(c);
}

int Tout::get_nb_client()
{
    return vecClients.size();
}

int Tout::consult_client(string n)
{
    for(auto elm : vecClients)
    {
        if(elm->get_nom() == n)
            elm->Clients::affiche();
            elm->Clients::affiche_detail();
            cout << endl << "---------------------------------------" << endl;
    }
    return 0;
}

void Tout::supp_client(string n)
{
    for(auto elm : vecClients)
    {
        if(elm->get_nom() == n)
        {
            elm->Clients::affiche();
            elm->Clients::affiche_detail();
            cout << endl << "---------------------------------------" << endl;
        }
    }
}



