#include <iostream>
using namespace std;

#ifndef EXCEPTIONS_H_INCLUDED
#define EXCEPTIONS_H_INCLUDED

class Exceptions : public exception
{
    private:
        string cause;
    public:
        Exceptions(string c) : cause(c) {}
        ~Exceptions() {}

        const char* what()
        {
            return cause.c_str();
        }

        string message()
        {
             return cause;
        }
};

#endif // EXCEPTIONS_H_INCLUDED
