#include <iostream>
#include <string>
#ifndef PROFESSIONELS_H_INCLUDED
#define PROFESSIONELS_H_INCLUDED
#include "Adresse.h"
#include "Clients.h"
using namespace std;

class Professionels : public Clients
{
    private:
        string siret;
        string raison_sociale;
        int annee_creation; //aaaa
        Adresse* adress_entreprise;
        string mail;

    public:
        string get_siret() { return siret; }
        void set_siret(string);
        string get_raisonsoc() { return raison_sociale; }
        void set_raisonsoc(string r) { raison_sociale = r; }
        int get_annee_creat() { return annee_creation; }
        void set_annee_creat(int);
        string get_mail() { return mail; }
        void set_mail(string);

        void affiche();
        virtual void affiche_detail() override;

        Professionels(string="\0", string="\0", char='M', int=0, string="\0", string="\0", int=0, string="\0", string="\0", string="\0", int=0, string="\0", string="\0", string="\0", int=0, string="\0");
        ~Professionels() {};
};

#endif // PROFESSIONELS_H_INCLUDED
