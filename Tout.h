#ifndef TOUT_H_INCLUDED
#define TOUT_H_INCLUDED
#include <vector>
#include "Clients.h"

class Clients;
class Tout
{
    private:
        vector<Clients*> vecClients;

    public:
         void affiche_tout();
         int get_nb_client();
         void ajoute_client(Clients*);
         int consult_client(string);
         void supp_client(string);
};

#endif // TOUT_H_INCLUDED
