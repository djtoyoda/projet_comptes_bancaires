#ifndef CLIENTS_H_INCLUDED
#define CLIENTS_H_INCLUDED
#include "Adresse.h"
#include "Compte.h"
#include <vector>
using namespace std;

class Compte;

class Clients
{
    private:
        string nom;
        string prenom;
        char sex;
        int telephone;
        Adresse* adresse_client=nullptr;
        vector<Compte*> vecComptes;

    public:
        string get_nom() { return nom; }
        void set_nom(string n) { nom = n; }
        string get_prenom() { return prenom; }
        void set_prenom(string p) { prenom = p; }
        char get_sex() { return sex; }
        void set_sex(char s) { sex = s; }
        int get_telephone() { return telephone; }
        void set_telephone(int t) { telephone = t; }

        void affiche();
        virtual void affiche_detail();
        void ajoute_compte(Compte*);
        void suppr_compte(Compte*);
        int get_nb_comptes();

        Clients(string="\0", string="\0", char='M', int=0, string="\0", string="\0", int=0, string="\0");
        ~Clients() {}
};

#endif // CLIENTS_H_INCLUDED
