#include <iostream>
#include <string>
#include "Professionels.h"
#include "Adresse.h"
#include "Exceptions.h"
using namespace std;

Professionels::Professionels(string n, string p, char s, int t, string lib, string c, int cp, string v, string sir, string r, int a, string m, string lib1, string c1, int cp1, string v1)
    :Clients(n, p, s, t, lib, c, cp, v)
{
    this->set_siret(sir);
    this->set_raisonsoc(r);
    this->set_annee_creat(a);
    this->set_mail(m);

    adress_entreprise = new Adresse(lib, c1, cp1, v1);
}

void Professionels::set_mail(string m)
{
    string arrobas = "@";
    if(m.find(arrobas) == string::npos)
    {
        throw Exceptions("L'adresse e-mail doit inclure un @.");
    }
    mail = m;
}
void Professionels::set_siret(string s)
{
    if(s.length() != 14)
    {
        throw Exceptions("Le siret doit contenir 14 chiffres");
    }
    siret = s;
}

void Professionels::set_annee_creat(int a)
{
    if(a< 1900 || a> 2020)
    {
        throw Exceptions("L'annee de creation doit etre entre 1900 et 2020");
    }
    annee_creation = a;
}

void Professionels::affiche()
{
    cout << "Contat Professionel" << endl;
    Clients::affiche();
    cout << "Siret: " << this->get_siret() << ", Raison Sociale: " << this->get_raisonsoc() << ", Annee creation: " << this-> get_annee_creat() << ", mail: " << this->get_mail() << endl;
    adress_entreprise->Adresse::affiche();
}

void Professionels::affiche_detail()
{
    cout << "prof aff det" << endl;
}
