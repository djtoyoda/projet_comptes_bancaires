#include <iostream>
#include "Compte.h"
#include "Exceptions.h"
using namespace std;

Compte::Compte(int n, int d, float s, float dcv)
{
    this->set_num_compte(n);
    this->set_date_ouv_compte(d);
    this->set_solde(s);
    this->set_decouv(dcv);
}

void Compte::affiche()
{
    cout << "Num compte: " << this->get_num_compte() << ", Date ouverture compte: " << this->get_date_ouv_compte() << ", Solde: " << this->get_solde() << endl;
}

void Compte::retrait(float r)
{
    if((this->get_solde() - r) < (this->get_decouv() *(-1)))
    {
        throw Exceptions("Retrait non autorise: solde insuffisant");
    }
    this->set_solde(this->get_solde() - r);
}

void Compte::paimentCB(float p)
{
    if((this->get_solde() - p) < (this->get_decouv() *(-1)))
    {
        throw Exceptions("Paiment non autorise: solde insuffisant");
    }
    this->set_solde(this->get_solde() - p);
}

void Compte::depot(float d)
{
    this->set_solde(this->get_solde() + d);
}
