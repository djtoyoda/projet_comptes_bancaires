#include <iostream>
#include <string>
#include "Professionels.h"
#include "Adresse.h"
using namespace std;

Professionels::Professionels(string n, string p, char s, int t, string lib, string comp, int cp, string v, int sir, string r, int a, string m, string l, string c, int cp1, string v1)
    :Clients(n, p, s, t, lib, c, cp, v)
{
    this->set_siret(sir);
    this->set_raisonsoc(r);
    this->set_annee_creat(a);
    this->set_mail(m);

    adress_entreprise = new Adresse(l, comp, cp1, v1);
}

void Professionels::affiche_detail()
{
    cout << "Siret: " << this->get_siret() << ", Raison Sociale: " << this->get_raisonsoc() << ", Annee creation: " << this-> get_annee_creat() << ", mail: " << this->get_mail() << endl;
    adress_entreprise->Adresse::affiche();
}
