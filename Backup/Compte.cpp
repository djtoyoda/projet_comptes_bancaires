#include <iostream>
#include "Compte.h"
using namespace std;

Compte::Compte(int n, int d, float s, float dcv)
{
    this->set_num_compte(n);
    this->set_date_ouv_compte(d);
    this->set_solde(s);
    this->set_decouv(dcv);
}

void Compte::affiche()
{
    cout << "Num compte: " << this->get_num_compte() << ", Date ouverture compte: " << this->get_date_ouv_compte() << endl;
}
