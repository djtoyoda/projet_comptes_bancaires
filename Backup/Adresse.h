#ifndef ADRESSE_H_INCLUDED
#define ADRESSE_H_INCLUDED
#include <string>
using namespace std;

class Adresse
{
    private:
        string libelle;
        string complement;
        int code_post;
        string ville;
    public:
        string get_libelle() { return libelle; }
        void set_libelle(string l) { libelle = l; }
        string get_complem() { return complement; }
        void set_complem(string c) { complement = c; }
        int get_codepost() { return code_post; }
        void set_codepost(int cp) { code_post = cp; }
        string get_ville() { return ville; }
        void set_ville(string v) { ville = v; }

        void affiche();

        Adresse(string="\0", string="\0", int=0, string="\0");
        ~Adresse() {};
};


#endif // ADRESSE_H_INCLUDED
