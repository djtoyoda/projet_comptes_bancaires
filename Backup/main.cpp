#include <iostream>
#include <string>
#include "Clients.h"
#include "Compte.h"
#include "Particuliers.h"
#include "Professionels.h"
#include "Tout.h"
using namespace std;

void consult_solde()
{

}

void ajouter_client()
{

}

void supp_client()
{

}

void mod_client()
{

}

void ajouter_op()
{

}

void supp_op()
{

}

void mod_op()
{

}

void afficher_op()
{

}

void importer_op()
{

}

int main()
{
    Tout tout;

    Clients* part1 = new Particuliers("Jackson", "John", 'M', 8983188, "Blv", "Emile Zola", 31300, "Toulouse", Sit_fam::Marie, 2879219);
    Clients* pro1 = new Professionels("Renault", "Mike", 'M', 84984561, "Rue", "Flambere", 32700, "Bordeaux", 5673, "AKKA", 2000, "akka@akka.eu", "Blvd", "Thomas Ziegler", 31300, "Beauzelle");

    tout.ajoute_client(part1);
    tout.ajoute_client(pro1);

    Compte c1(123, 7032020, 50.5, 0);
    Compte c2(568, 8062019, 100.5, 100);
    //Compte c3 = new Compte(698, 9506807, 120, 50);
    Compte c3(698, 9506807, 120, 50);

    part1->ajoute_compte(c1);
    pro1->ajoute_compte(c2);
    pro1->ajoute_compte(c3);

    tout.affiche_tout();

    cout << "------------------------------------------" << endl;
    cout << "%                                        %" << endl;
    cout << "%                                        %" << endl;
    cout << "%            GESTION DE COMPTES          %" << endl;
    cout << "%                                        %" << endl;
    cout << "%                                        %" << endl;
    cout << "------------------------------------------" << endl << endl;
    cout << "------------------- MENU -----------------" << endl;
    cout << "Choisissez l'option:" << endl;
    cout << "1- Lister l'ensemble des clients" << endl;
    cout << "2- Consulter les soldes des comptes pour un client donne" << endl;
    cout << "3- Ajouter/Supprimer/Modifier un client" << endl;
    cout << "4- Ajouter/Supprimer/Modifier une operation" << endl;
    cout << "5- Afficher l'ensemble des operations pour un compte donne" << endl;
    cout << "6- Importer le fichier des operations bancaires" << endl << endl;
    cout << "Option: ";
    int inputX;
    cin >> inputX;

    while(inputX < 1 || inputX > 6)
    {
        cout << "Option invalide; choisissez entre 1 e 6: ";
        cin >> inputX;
    }

    if(inputX==1)
    {
        tout.affiche_tout();
    }
    if(inputX==2)
    {
        cout << "Donnez les nom du client: ";
        string input2X;
        //cin >> input2X;
        input2X = "Renault";
        int cons = tout.consult_client(input2X);
        cout <<  "Cons " << cons;
        //int comp[] = tout.consult_compte(cons);
    }
    if(inputX==3)
    {
        cout << "Choisissez l'option:" << endl;
        cout << "1- Ajouter un client" << endl;
        cout << "2- Supprimer un client" << endl;
        cout << "3- Modifier un client" << endl;
        int input3X;
        cin >> input3X;

            while(input3X < 1 || input3X > 3)
            {
                cout << "Option invalide; choisissez entre 1 e 3: ";
                cin >> input3X;
            }

            if(input3X==1)
            {
            }
    }
    if(inputX==4)
    {

    }
    if(inputX==5)
    {

    }
    if(inputX==6)
    {

    }

    return 0;
}
