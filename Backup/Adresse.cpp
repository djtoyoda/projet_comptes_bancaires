#include <iostream>
#include "Adresse.h"
using namespace std;

Adresse::Adresse(string l, string c, int cp, string v)
{
    this->set_libelle(l);
    this->set_complem(c);
    this->set_codepost(cp);
    this->set_ville(v);
}

void Adresse::affiche()
{
    cout << "Adresse : " << this->get_libelle() << " " << this->get_complem() << " - " << this->get_codepost() << " " << this->get_ville() << endl;
}

