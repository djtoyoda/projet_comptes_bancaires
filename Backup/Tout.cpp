#include "Tout.h"
#include <iostream>
#include <vector>

using namespace std;

void Tout::affiche_tout()
{
    cout << "---------------- Nb Clients : " << this->get_nb_client() << " clients ---------------" << endl << endl;
    for(auto elm : vecClients)
    {
        elm->Clients::affiche();
    }
}

void Tout::ajoute_client(Clients* c)
{
    vecClients.push_back(c);
}

int Tout::get_nb_client()
{
    return vecClients.size();
}

int Tout::consult_client(string nom_consult)
{
    cout << "Nb client " << get_nb_client() << endl;
    for(int i=0; i< get_nb_client(); i++)
    {
        if(vecClients[i]->Clients::get_nom() == nom_consult)
        {
            cout << i << " " << vecClients[i]->Clients::get_nom() << endl;
            //cout << i << " " << vecComptes[0]->Comptes::get_solde() << endl;
            return i;
        }
    }
    return 0;
}
