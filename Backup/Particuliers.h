#include <iostream>
#include <string>
#include "Clients.h"
#ifndef PARTICULIERS_H_INCLUDED
#define PARTICULIERS_H_INCLUDED
using namespace std;

enum class Sit_fam {Celibataire, Marie, Divorce, Autre};

class Particuliers : public Clients
{
    private:
        Sit_fam sit_fam;
        int date_naissance; //jjmmaaaa

    public:
        string get_sitfam();
        void set_sitfam(Sit_fam s) { sit_fam = s; }
        int get_datenaiss() { return date_naissance; }
        void set_datenaiss(int d) { date_naissance = d; }

        void affiche_detail() override;

        Particuliers(string="\0", string="\0", char='M', int=0, string="\0", string="\0", int=0, string="\0", Sit_fam=Sit_fam::Autre, int=0);
        ~Particuliers() {};
};

#endif // PARTICULIERS_H_INCLUDED
