#include <iostream>
#include <string>
#include "Particuliers.h"
#include "Adresse.h"
using namespace std;

Particuliers::Particuliers(string n, string p, char s, int t, string l, string c, int cp, string v, Sit_fam sf, int d)
    :Clients(n, p, s, t, l, c, cp, v)
{
    this->set_datenaiss(d);
    this->set_sitfam(sf);
}

string Particuliers::get_sitfam()
{
    string result="\0";
    switch(sit_fam)
    {
        case Sit_fam::Celibataire:
            result= "Celibataire";
            break;
        case Sit_fam::Marie:
            result= "Marie(e)";
            break;
        case Sit_fam::Divorce:
            result= "Divorce(e)";
            break;
        default:
            result= "Autre";
            break;
    }
    return result;
}

void Particuliers::affiche_detail()
{
    cout << "Situation familiale: " << this->get_sitfam() << ", Date naissance: " << this->get_datenaiss() << endl;
}


