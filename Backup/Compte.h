#ifndef COMPTE_H_INCLUDED
#define COMPTE_H_INCLUDED

class Compte
{
    private:
        int num_compte;
        int date_ouv_compte;
        float solde;
        float decouv_aut;

    public:
        int get_num_compte() { return num_compte; }
        void set_num_compte(int n) { num_compte = n; }
        int get_date_ouv_compte() { return date_ouv_compte; }
        void set_date_ouv_compte(int d) { date_ouv_compte = d; }
        float get_solde() { return solde; }
        void set_solde(float s) { solde = s; }
        float get_decouv() { return decouv_aut; }
        void set_decouv (float dcv) { decouv_aut = dcv; }

        void affiche();

        Compte(int=0, int=0, float=0.0, float=0.0);
        ~Compte() {};
};

#endif // COMPTE_H_INCLUDED
